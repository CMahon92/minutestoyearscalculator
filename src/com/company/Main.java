package com.company;

public class Main {

    public static void main(String[] args) {

        printYearsAndDays(700000);
    }
    public static void printYearsAndDays (long minutes) {
        if (minutes < 0) {
            System.out.println("Invalid Value");
        }else {

            long year = minutes / 525600;
            long remaningMinutes = minutes % 525600;
            long days = remaningMinutes / 1440;

            System.out.println(minutes + " min = " + year + " y and " + days + " d");
        }
    }
}
